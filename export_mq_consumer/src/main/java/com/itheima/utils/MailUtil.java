package com.itheima.utils;

import javax.mail.Address;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
public class MailUtil {

    public static void sengMsg(String to,String subject,String content)throws Exception{
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "smtp.sina.com"); //设置主机地址smtp.qq.com smtp.sina.com
        props.setProperty("mail.smtp.auth", "true");//认证
        //2.产生一个用于邮件发送的Session对象
        Session session = Session.getInstance(props);
        //3.产生一个邮件的消息对象
        MimeMessage message = new MimeMessage(session);
        //4.设置消息的发送者
        Address fromAddr = new InternetAddress("itcast004@sina.com");
        message.setFrom(fromAddr);
        //5.设置消息的接收者
        Address toAddr = new InternetAddress(to);
        //TO 直接发送 CC抄送 BCC密送
        message.setRecipient(MimeMessage.RecipientType.TO, toAddr);
        //6.设置主题
        message.setSubject(subject);
        //7.设置正文
        message.setText(content);

        //8.准备发送，得到火箭
        Transport transport = session.getTransport("smtp");
        //9.设置火箭的发射目标
        transport.connect("smtp.sina.com", "itcast004@sina.com", "loveyou");
        //10.发送
        transport.sendMessage(message, message.getAllRecipients());
        //11.关闭
        transport.close();

    }
    /*public static void sengMsg(String to,String subject,String content)throws Exception{
        Properties properties = new Properties();
        properties.setProperty("mail.163.host","smtp.163.com");
        properties.setProperty("mail.163.auth","true");
        Session session = Session.getInstance(properties);
        MimeMessage message = new MimeMessage(session);
        InternetAddress address = new InternetAddress("xiaohuajianke@163.com");
        message.setFrom(address);
        InternetAddress toAddr = new InternetAddress(to);
        message.setRecipient(MimeMessage.RecipientType.TO,toAddr);
        message.setSubject(subject);
        message.setText(content);
        Transport transport = session.getTransport("smtp");
        transport.connect("smtp.163.com","xiaohuajianke@163.com","czhd93032");
        transport.sendMessage(message,message.getAllRecipients());
        transport.close();
    }*/
}
