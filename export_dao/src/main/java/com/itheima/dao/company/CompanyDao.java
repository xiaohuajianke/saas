package com.itheima.dao.company;

import com.itheima.domain.company.Company;

import java.util.List;

/**
 * █████   ▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██    ▒██   ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 * ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 * ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 * ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 * ░     ░ ░      ░  ░
 */

public interface CompanyDao {
//    @Select("select * from ss_company")
//    @Results({
//            @Result(column = "id",property = "id",id = true),
//            @Result(property="name" ,column="name"),
//            @Result(property="expirationDate" ,column="expiration_date"),
//            @Result(property="address" ,column="address"),
//            @Result(property="licenseId" ,column="license_id"),
//            @Result(property="representative" ,column="representative"),
//            @Result(property="phone" ,column="phone"),
//            @Result(property="companySize" ,column="company_size"),
//            @Result(property="industry" ,column="industry"),
//            @Result(property="remarks" ,column="remarks"),
//            @Result(property="state" ,column="state"),
//            @Result(property="balance" ,column="balance"),
//            @Result(property="city" ,column="city")
//    })
    List<Company> findAll();

    int addCompany(Company company);

    int update(Company company);

    Company findCompanyById(String id);

    void deleteById(String id);

}
