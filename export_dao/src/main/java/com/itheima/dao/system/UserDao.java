package com.itheima.dao.system;

import com.itheima.domain.system.Module;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * █████   ▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██    ▒██   ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 * ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 * ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 * ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 * ░     ░ ░      ░  ░
 */
public interface UserDao {
    //根据企业id查询全部
    List<User> findAll(String companyId);
    //根据id查询
    User findById(String userId);
    //根据id删除
    void delete(String userId);
    //保存
    void save(User user);
    //更新
    void update(User user);

    Long findUserRoleByUserId(String id);

    void deleteUserRole(String userId);

    void saveUserRole(@Param("userId") String userId, @Param("roleId") String roleId);

    User findByEmail(String email);

}
