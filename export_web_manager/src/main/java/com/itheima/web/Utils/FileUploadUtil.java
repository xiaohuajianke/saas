package com.itheima.web.Utils;


import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Component
public class FileUploadUtil {
    private static final Logger logger = LoggerFactory.getLogger(FileUploadUtil.class);
    private static final String ACCESS_KEY = "OgO9ZNtigU37m04MPcH8XV4yMiYOdIspVAL8ykUg";
    private static final String SECRET_KEY = "jKUdwuoy83YRt8pIl3kloU9F4F-9RAOxbXR0kpOy";
    private static final String BUCKET = "export-banner";
    public static final String IMG_SUFFIX_LIST = "jpg,jpeg,gif,png,ico,bmp,jpeg";
    public static final String URL = "http://pztdy8om8.bkt.clouddn.com/";
    /**
     * 上传图片
     * @param file
     * @return
     * @throws IOException
     */
    public static String upload(@RequestParam MultipartFile file) {
        if (file == null) {
            logger.warn("上传文件为空!");return null;
        }
        InputStream inputStream = null;
        try {
            /** 获取文件的后缀* */
            if (!checkFile(file.getOriginalFilename())) {
                logger.warn("上传文件类型不支持");
                return null;
            }
            inputStream = file.getInputStream();
            DefaultPutRet ret = qiNiuUpload(inputStream);
            if (null == ret) {
                logger.error("上传文件服务器返回为null");
                return null;
            }
            logger.info("上传图片名称:" + file.getOriginalFilename());
            logger.info("上传后hash:" + ret.hash);
            logger.info("上传后key:" + ret.key);
            return URL+ret.hash;
        } catch (QiniuException ex) {
            logger.error("上传七牛服务器异常", ex);
        } catch (Exception e) {
            logger.error("上传异常", e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error("上传文件，关闭输入流异常", e);
            }
        }
        return null;
    }
    /**
     * 检测图片文件类型
     * @param fileName
     * @return
     */
    public static boolean checkFile(String fileName) {
        boolean flag = false;
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        logger.info("上传文件后缀:" + suffix);
        if (IMG_SUFFIX_LIST.contains(suffix.trim().toLowerCase())) {
            flag = true;
        }
        return flag;
    }
    /**
     * 上传七牛服务器
     * @param is
     * @return
     */
    private static DefaultPutRet qiNiuUpload(InputStream is) throws Exception {
        Configuration cfg = new Configuration(Zone.zone0());
        UploadManager uploadManager = new UploadManager(cfg);
        Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
        String upToken = auth.uploadToken(BUCKET);
        com.qiniu.http.Response response = uploadManager.put(is, null, upToken, null, null);
        //解析上传成功的结果
        DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
        logger.info(putRet.toString());
        logger.info(putRet.key);
        logger.info(putRet.hash);
        return putRet;
    }
}
