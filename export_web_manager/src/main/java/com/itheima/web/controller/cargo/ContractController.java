package com.itheima.web.controller.cargo;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.cargo.Contract;
import com.itheima.domain.cargo.ContractExample;
import com.itheima.service.cargo.ContractService;
import com.itheima.web.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/cargo/contract")
public class ContractController extends BaseController {

    @Reference(retries = 0,check = false)
    private ContractService contractService;


    /**
     * 购销合同列表分页
     */
    @RequestMapping("/list")
    public String list(
            @RequestParam(defaultValue = "1") int pageNum,
            @RequestParam(defaultValue = "5") int pageSize) {
        // 构造条件
        ContractExample example = new ContractExample();
        // 构造条件-排序
        example.setOrderByClause("create_time desc");
        ContractExample.Criteria criteria = example.createCriteria();
        // 构造条件-根据公司id查询
        criteria.andCompanyIdEqualTo(getLoginCompanyId());

        if (getLoginUser().getDegree()==4){
            criteria.andCreateByEqualTo(getLoginUser().getId());
        }if (getLoginUser().getDegree()==3){
            criteria.andCreateDeptEqualTo(getLoginUser().getDeptId());
        }if (getLoginUser().getDegree()==2){
            request.setAttribute("pageInfo",contractService.selectByDeptId(getLoginUser().getDeptId(),pageNum,pageSize));
            return "cargo/contract/contract-list";
        }

        //1.调用service查询购销合同列表
        PageInfo<Contract> pageInfo = contractService.findByPage(example, pageNum, pageSize);
        //2.保存数据
        request.setAttribute("pageInfo",pageInfo);
        //3. 返回
        return "cargo/contract/contract-list";
    }
    /**
     * 进入新增页面
     */
    @RequestMapping("/toAdd")
    public String toAdd() {
        return "cargo/contract/contract-add";
    }
    /**
     * 新增或修改
     */
    @RequestMapping("/edit")
    public String edit(Contract contract) {
        contract.setCompanyId(super.getLoginCompanyId());
        contract.setCompanyName(super.getLoginCompanyName());
        contract.setUpdateTime(new Date());
        //1.判断是否具有id属性
        if(StringUtils.isEmpty(contract.getId())) {
        //2.没有id，保存
            contract.setCreateBy(getLoginUser().getId());
            contract.setCreateDept(getLoginUser().getDeptId());

            contractService.save(contract);
        }else{
        //3.有id，更新
            contractService.update(contract);
        }
        return "redirect:/cargo/contract/list.do";
    }
    /**
     * 进入到修改界面
     */
    @RequestMapping("/toUpdate")
    public String toUpdate(String id) {
        //根据id进行查询
        Contract contract = contractService.findById(id);
        //保存数据
        request.setAttribute("contract",contract);
        return "cargo/contract/contract-update";
    }
    /**
     * 删除
     */
    @RequestMapping("/delete")
    public String delete(String id) {
        contractService.delete(id);
        //跳转到修改界面
        return "redirect:/cargo/contract/list.do";
    }

    @RequestMapping("/toView")
    public String goView(String id){
        Contract contract = contractService.findById(id);
        request.setAttribute("contract",contract);
        return "cargo/contract/contract-view";
    }

    @RequestMapping("submit")
    public String submit(String id){
        Contract contract = new Contract();
        contract.setId(id);
        contract.setState(1);
        contractService.update(contract);
        return "redirect:/cargo/contract/list.do";
    }

    @RequestMapping("cancel")
    public String cancel(String id){
        Contract contract = new Contract();
        contract.setId(id);
        contract.setState(0);
        contractService.update(contract);
        return "redirect:/cargo/contract/list.do";
    }

}
