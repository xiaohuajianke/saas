package com.itheima.web.controller.cargo;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.domain.cargo.Export;
import com.itheima.domain.cargo.ExportProduct;
import com.itheima.domain.cargo.ExportProductExample;
import com.itheima.service.cargo.ExportProductService;
import com.itheima.service.cargo.ExportService;
import com.itheima.service.stat.StatService;
import com.itheima.web.Utils.BeanMapUtils;
import com.itheima.web.controller.BaseController;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/cargo/export")
public class PdfController extends BaseController {

    //jdbc数据源
    @Autowired
    private DataSource dataSource;
    @Reference(check = false)
    private ExportService exportService;
    @Reference(check = false)
    private ExportProductService exportProductService;



    @Reference
    private StatService statService;

    @RequestMapping("/exportPdf")
    public void exportPdf(String id) throws IOException, JRException {
        Export export = exportService.findById(id);
        Map<String, Object> map = BeanMapUtils.beanToMap(export);


        ExportProductExample exportProductExample = new ExportProductExample();
        exportProductExample.createCriteria().andExportIdEqualTo(id);
        List<ExportProduct> list = exportProductService.findAll(exportProductExample);
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);

        InputStream in = session.getServletContext().getResourceAsStream("/jasper/export.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(in, map, dataSource);

        JasperExportManager.exportReportToPdfStream(jasperPrint,response.getOutputStream());


    }


    /*@RequestMapping("/exportPdf")
    public void exportPdf() {

        try {
            InputStream inputStream = session.getServletContext().getResourceAsStream("/jasper/chartTest.jasper");
            //InputStream inputStream = session.getServletContext().getResourceAsStream("/jasper/test01.jasper");
            //InputStream inputStream = Resources.getResourceAsStream("jdbcTest.jasper");
            System.out.println("inputStream----------------->"+inputStream);


            *//*HashMap<String, Object> map = new HashMap<>();
            map.put("username",getLoginUser().getUserName());
            map.put("email",getLoginUser().getEmail());
            map.put("companyName",getLoginUser().getCompanyName());
            map.put("deptName",getLoginUser().getDeptName());*//*


            javaBean数据源
            ArrayList<User> list = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 5; j++) {
                    User user = new User();
                    user.setUserName("name"+j);
                    user.setEmail(j+"@qq.com");
                    user.setCompanyName("企业"+i);
                    user.setDeptName("部门"+j);
                    list.add(user);
                }
            }*//*

            //测试
            ArrayList<Map<String, Object>> list = new ArrayList<>();
            List<Map<String, Object>> statList = statService.getSellData(getLoginCompanyId());
            for (Map<String, Object> stringObjectMap : statList) {
                HashMap<String, Object> map1 = new HashMap<>();
                map1.put("title",(String) stringObjectMap.get("name"));
                map1.put("value",(int)(((BigDecimal)stringObjectMap.get("value")).doubleValue()));
                list.add(map1);

                System.out.println( (String) stringObjectMap.get("name")+":"+((BigDecimal)stringObjectMap.get("value")).doubleValue());
            }

            *//*for (int i = 0; i < statList.size(); i++) {
                int a=i;
                try {
                    HashMap<String, Object> map = new HashMap<>();
                    Map<String, Object> map1 = statList.get(i++);
                    map.put("title",map1.get("name"));
                    Map<String, Object> map2 = statList.get(i);
                    map.put("value", Integer.parseInt((String)map2.get("value")));
                    list.add(map);
                    System.out.println(map);
                }catch (Exception e){
                    e.printStackTrace();
                    if (a==i){
                        i++;
                    }
                }

            }*//*

            //讲义的测试数据
            *//*ArrayList<Object> list = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                HashMap<Object, Object> map = new HashMap<>();
                map.put("title","title"+i);
                map.put("value",new Random().nextInt(100));
                list.add(map);
            }*//*

            JRBeanCollectionDataSource dataSource1 = new JRBeanCollectionDataSource(list);

            System.out.println("dataSource------------------->"+ this.dataSource);
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream,new HashMap<>(), dataSource1);
            JasperExportManager.exportReportToPdfStream(jasperPrint,response.getOutputStream());
        }catch (Exception e){
            e.printStackTrace();
        }
        //return "redirect:/cargo/export/list.do";


    }*/
}
