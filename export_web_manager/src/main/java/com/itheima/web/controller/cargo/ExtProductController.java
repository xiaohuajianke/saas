package com.itheima.web.controller.cargo;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.itheima.domain.cargo.ExtCproduct;
import com.itheima.domain.cargo.ExtCproductExample;
import com.itheima.domain.cargo.Factory;
import com.itheima.domain.cargo.FactoryExample;
import com.itheima.service.cargo.ExtCproductService;
import com.itheima.service.cargo.FactoryService;
import com.itheima.web.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.UUID;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/cargo/extCproduct")
public class ExtProductController extends BaseController {

    @Reference(check = false,retries = 0)
    private FactoryService factoryService;
    @Reference(check = false,retries = 0)
    private ExtCproductService extCproductService;

    @RequestMapping("/list")
    public String goList(String contractId, String contractProductId,
                         @RequestParam(defaultValue = "5")int pageSize,
                         @RequestParam(defaultValue = "1")int pageNum){

        FactoryExample factoryExample = new FactoryExample();
        factoryExample.createCriteria().andCtypeEqualTo("附件");
        List<Factory> factoryList = factoryService.findAll(factoryExample);
        request.setAttribute("factoryList",factoryList);

        ExtCproductExample extCproductExample = new ExtCproductExample();
        extCproductExample.createCriteria().andContractProductIdEqualTo(contractProductId);
        PageInfo<ExtCproduct> pageInfo = extCproductService.findByPage(extCproductExample, pageNum, pageSize);
        request.setAttribute("pageInfo",pageInfo);
        request.setAttribute("contractId",contractId);
        request.setAttribute("contractProductId",contractProductId);
        return "cargo/extc/extc-list";
    }

    @RequestMapping("edit")
    public String edit(ExtCproduct extCproduct){
        extCproduct.setCompanyId(getLoginCompanyId());
        extCproduct.setCompanyId(getLoginCompanyName());
        if (StringUtil.isEmpty(extCproduct.getId())){
            extCproduct.setId(UUID.randomUUID().toString());
            extCproductService.save(extCproduct);
        }else {
            extCproductService.update(extCproduct);
        }
        return "redirect:/cargo/extCproduct/list.do?contractId="+extCproduct.getContractId()+"&contractProductId="+extCproduct.getContractProductId();
    }

    @RequestMapping("/toUpdate")
    public String goUpdate(String id,String contractId,String contractProductId){
        ExtCproduct extCproduct = extCproductService.findById(id);
        FactoryExample factoryExample = new FactoryExample();
        factoryExample.createCriteria().andCtypeEqualTo("附件");
        List<Factory> factoryList = factoryService.findAll(factoryExample);
        request.setAttribute("extCproduct",extCproduct);
        request.setAttribute("factoryList",factoryList);
        request.setAttribute("contractId",contractId);
        request.setAttribute("contractProductId",contractProductId);
        return "cargo/extc/extc-update";
    }

    @RequestMapping("/delete")
    public String delete(String id,String contractId,String contractProductId){
        extCproductService.delete(id);
        return "redirect:/cargo/extCproduct/list.do?contractId="+contractId+"&contractProductId="+contractProductId;
    }
}
