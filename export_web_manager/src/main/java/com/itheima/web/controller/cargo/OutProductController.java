package com.itheima.web.controller.cargo;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.domain.vo.ContractProductVo;
import com.itheima.service.cargo.ContractProductService;
import com.itheima.web.controller.BaseController;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/cargo/contract")
public class OutProductController extends BaseController {
    @Reference
    private ContractProductService contractProductService;

    @RequestMapping("/print")
    public String print() {
        return "cargo/print/contract-print";
    }

    @RequestMapping("/printExcel")
    public void printExcell(String inputDate) throws IOException {
        //String realPath = session.getServletContext().getRealPath("/make/");
        //System.out.println("realPath----------------"+realPath);
        //InputStream in = new FileInputStream(realPath+"xlsprint\\tOUTPRODUCT.xlsx");
        InputStream in=session.getServletContext().getResourceAsStream("/make/xlsprint/tOUTPRODUCT.xlsx");
        //InputStream in=session.getServletContext().getResourceAsStream("classpath*:tOUTPRODUCT.xlsx");

        //InputStream in = Resources.getResourceAsStream("tOUTPRODUCT.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(in);

        XSSFSheet sheet = workbook.getSheetAt(0);

        System.out.println("表格为" + workbook);
        XSSFRow row = sheet.getRow(2);
        CellStyle[] cellStyles = new CellStyle[8];
        for (int i = 0; i < 8; i++) {
            cellStyles[i] = row.getCell(i + 1).getCellStyle();
        }
        String value = inputDate.replace("-0", "-").replace("-", "年") + "月份出货表";
        sheet.getRow(0).getCell(1).setCellValue(value);

        List<ContractProductVo> list = contractProductService.findByShipTime(getLoginCompanyId(), inputDate);
        int index = 2;
        XSSFCell cell;
        for (ContractProductVo contractProductVo : list) {
            int i = 0;
            row = sheet.createRow(index++);
            cell = row.createCell(1);
            cell.setCellValue(contractProductVo.getCustomName());
            cell.setCellStyle(cellStyles[i++]);

            cell = row.createCell(2);
            cell.setCellValue(contractProductVo.getContractNo());
            cell.setCellStyle(cellStyles[i++]);
            cell = row.createCell(3);
            cell.setCellValue(contractProductVo.getProductNo());
            cell.setCellStyle(cellStyles[i++]);
            cell = row.createCell(4);
            cell.setCellValue(contractProductVo.getCnumber());
            cell.setCellStyle(cellStyles[i++]);
            cell = row.createCell(5);
            cell.setCellValue(contractProductVo.getFactoryName());
            cell.setCellStyle(cellStyles[i++]);
            cell = row.createCell(6);
            cell.setCellValue(new SimpleDateFormat("yyyy-MM-dd").
                    format(contractProductVo.getDeliveryPeriod()));
            cell.setCellStyle(cellStyles[i++]);
            cell = row.createCell(7);
            cell.setCellValue(
                    new SimpleDateFormat("yyyy-MM-dd").
                            format(contractProductVo.getShipTime()));
            cell.setCellStyle(cellStyles[i++]);
            cell = row.createCell(8);
            cell.setCellValue(contractProductVo.getTradeTerms());
            cell.setCellStyle(cellStyles[i]);
        }

        response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(value + ".xlsx", "UTF-8"));
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();

    }

    /*@RequestMapping("printExcel1")
    public void printExcell1(String inputDate) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("出货表");
        sheet.setColumnWidth(0,5*256);
        sheet.setColumnWidth(1, 26 * 256);
        sheet.setColumnWidth(2, 12 * 256);
        sheet.setColumnWidth(3, 30 * 256);
        sheet.setColumnWidth(4, 12 * 256);
        sheet.setColumnWidth(5, 15 * 256);
        sheet.setColumnWidth(6, 10 * 256);
        sheet.setColumnWidth(7, 10 * 256);
        sheet.setColumnWidth(8, 8 * 256);

        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));

        XSSFRow row = sheet.createRow(0);
        row.setHeightInPoints(36);
        XSSFCell cell = row.createCell(1);
        cell.setCellStyle(this.bigTitle(workbook));
        String value=inputDate.replace("-0","0").replace("-","年")+"月份出货表";
        cell.setCellValue(value);

        row = sheet.createRow(1);
        row.setHeightInPoints(26);
        String[] title=new String[]{"客户", "订单号", "货号", "数量", "工厂", "工厂交期", "船期", "贸易条款"};
        for (int i = 0; i < title.length; i++) {
            cell = row.createCell(i + 1);
            cell.setCellValue(title[i]);
            cell.setCellStyle(this.title(workbook));

        }


        List<ContractProductVo> list = contractProductService.findByShipTime(getLoginCompanyId(), inputDate);
        if (list!=null&&list.size()!=0){
            int index=2;
            for (ContractProductVo contractProductVo : list) {
                row = sheet.createRow(index++);
                row.setHeightInPoints(24);
                cell= row.createCell(1);
                cell.setCellValue(contractProductVo.getCustomName());
                cell.setCellStyle(this.text(workbook));
                cell = row.createCell(2);
                cell.setCellValue(contractProductVo.getContractNo());
                cell.setCellStyle(this.text(workbook));
                cell = row.createCell(3);
                cell.setCellValue(contractProductVo.getProductNo());
                cell.setCellStyle(this.text(workbook));
                cell = row.createCell(4);
                cell.setCellValue(contractProductVo.getCnumber());
                cell.setCellStyle(this.text(workbook));
                cell = row.createCell(5);
                cell.setCellValue(contractProductVo.getFactoryName());
                cell.setCellStyle(this.text(workbook));
                cell = row.createCell(6);
                cell.setCellValue(new SimpleDateFormat("yyyy-MM-dd").
                        format(contractProductVo.getDeliveryPeriod()));
                cell.setCellStyle(this.text(workbook));
                cell = row.createCell(7);
                cell.setCellValue(
                        new SimpleDateFormat("yyyy-MM-dd").
                                format(contractProductVo.getShipTime()));
                cell.setCellStyle(this.text(workbook));
                cell = row.createCell(8);
                cell.setCellValue(contractProductVo.getTradeTerms());
                cell.setCellStyle(this.text(workbook));
            }
        }
        //new DownloadUtil().download(bos,response,"出货表.xlsx");
        response.setHeader("content-disposition", "attachment;filename="+ URLEncoder.encode(value+".xlsx", "UTF-8"));
        OutputStream outputStream=response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }
    //大标题的样式
    public CellStyle bigTitle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short)16);
        font.setBold(true);//字体加粗
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER); //横向居中
        style.setVerticalAlignment(VerticalAlignment.CENTER); //纵向居中
        return style;
    }

    //小标题的样式
    public CellStyle title(Workbook wb){
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontName("黑体");
        font.setFontHeightInPoints((short)12);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER); //横向居中
        style.setVerticalAlignment(VerticalAlignment.CENTER); //纵向居中
        style.setBorderTop(BorderStyle.THIN); //上细线
        style.setBorderBottom(BorderStyle.THIN); //下细线
        style.setBorderLeft(BorderStyle.THIN); //左细线
        style.setBorderRight(BorderStyle.THIN); //右细线
        return style;
    }

    //文字样式
    public CellStyle text(Workbook wb){
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontName("Times New Roman");
        font.setFontHeightInPoints((short)10);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.LEFT); //横向居左
        style.setVerticalAlignment(VerticalAlignment.CENTER); //纵向居中
        style.setBorderTop(BorderStyle.THIN); //上细线
        style.setBorderBottom(BorderStyle.THIN); //下细线
        style.setBorderLeft(BorderStyle.THIN); //左细线
        style.setBorderRight(BorderStyle.THIN); //右细线
        return style;
    }*/


    //百万数据导出测试
/*

    @RequestMapping("printExcel")
    public void printExcell(String inputDate) throws IOException {
        SXSSFWorkbook workbook = new SXSSFWorkbook();
        SXSSFSheet sheet = workbook.createSheet("出货表");
        sheet.setColumnWidth(0,5*256);
        sheet.setColumnWidth(1, 26 * 256);
        sheet.setColumnWidth(2, 12 * 256);
        sheet.setColumnWidth(3, 30 * 256);
        sheet.setColumnWidth(4, 12 * 256);
        sheet.setColumnWidth(5, 15 * 256);
        sheet.setColumnWidth(6, 10 * 256);
        sheet.setColumnWidth(7, 10 * 256);
        sheet.setColumnWidth(8, 8 * 256);

        sheet.addMergedRegion(new CellRangeAddress(0,0,1,8));

        SXSSFRow row = sheet.createRow(0);
        row.setHeightInPoints(36);
        SXSSFCell cell = row.createCell(1);
        cell.setCellStyle(this.bigTitle(workbook));
        String value=inputDate.replace("-0","0").replace("-","年")+"月份出货表";
        cell.setCellValue(value);

        row = sheet.createRow(1);
        row.setHeightInPoints(26);
        String[] title=new String[]{"客户", "订单号", "货号", "数量", "工厂", "工厂交期", "船期", "贸易条款"};
        for (int i = 0; i < title.length; i++) {
            cell = row.createCell(i + 1);
            cell.setCellValue(title[i]);
            cell.setCellStyle(this.title(workbook));

        }


        List<ContractProductVo> list = contractProductService.findByShipTime(getLoginCompanyId(), inputDate);
        if (list!=null&&list.size()!=0){
            int index=2;
            for (ContractProductVo contractProductVo : list) {
                for (int i = 0; i < 6000; i++) {
                    row = sheet.createRow(index++);
                    row.setHeightInPoints(24);
                    cell= row.createCell(1);
                    cell.setCellValue(contractProductVo.getCustomName());
                    //cell.setCellStyle(this.text(workbook));
                    cell = row.createCell(2);
                    cell.setCellValue(contractProductVo.getContractNo());
                    //cell.setCellStyle(this.text(workbook));
                    cell = row.createCell(3);
                    cell.setCellValue(contractProductVo.getProductNo());
                    //cell.setCellStyle(this.text(workbook));
                    cell = row.createCell(4);
                    cell.setCellValue(contractProductVo.getCnumber());
                    //cell.setCellStyle(this.text(workbook));
                    cell = row.createCell(5);
                    cell.setCellValue(contractProductVo.getFactoryName());
                    //cell.setCellStyle(this.text(workbook));
                    cell = row.createCell(6);
                    cell.setCellValue(new SimpleDateFormat("yyyy-MM-dd").
                            format(contractProductVo.getDeliveryPeriod()));
                    //cell.setCellStyle(this.text(workbook));
                    cell = row.createCell(7);
                    cell.setCellValue(
                            new SimpleDateFormat("yyyy-MM-dd").
                                    format(contractProductVo.getShipTime()));
                    //cell.setCellStyle(this.text(workbook));
                    cell = row.createCell(8);
                    cell.setCellValue(contractProductVo.getTradeTerms());
                    //cell.setCellStyle(this.text(workbook));
                }
            }
        }
        //new DownloadUtil().download(bos,response,"出货表.xlsx");
        response.setHeader("content-disposition", "attachment;filename="+ URLEncoder.encode(value+".xlsx", "UTF-8"));
        OutputStream outputStream=response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }
    //大标题的样式
    public CellStyle bigTitle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short)16);
        font.setBold(true);//字体加粗
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER); //横向居中
        style.setVerticalAlignment(VerticalAlignment.CENTER); //纵向居中
        return style;
    }

    //小标题的样式
    public CellStyle title(Workbook wb){
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontName("黑体");
        font.setFontHeightInPoints((short)12);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER); //横向居中
        style.setVerticalAlignment(VerticalAlignment.CENTER); //纵向居中
        style.setBorderTop(BorderStyle.THIN); //上细线
        style.setBorderBottom(BorderStyle.THIN); //下细线
        style.setBorderLeft(BorderStyle.THIN); //左细线
        style.setBorderRight(BorderStyle.THIN); //右细线
        return style;
    }

    //文字样式
    public CellStyle text(Workbook wb){
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontName("Times New Roman");
        font.setFontHeightInPoints((short)10);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.LEFT); //横向居左
        style.setVerticalAlignment(VerticalAlignment.CENTER); //纵向居中
        style.setBorderTop(BorderStyle.THIN); //上细线
        style.setBorderBottom(BorderStyle.THIN); //下细线
        style.setBorderLeft(BorderStyle.THIN); //左细线
        style.setBorderRight(BorderStyle.THIN); //右细线
        return style;
    }

*/


}
