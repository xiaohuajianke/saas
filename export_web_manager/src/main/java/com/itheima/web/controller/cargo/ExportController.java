package com.itheima.web.controller.cargo;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.itheima.domain.cargo.*;
import com.itheima.domain.vo.ExportProductVo;
import com.itheima.domain.vo.ExportResult;
import com.itheima.domain.vo.ExportVo;
import com.itheima.service.cargo.ContractService;
import com.itheima.service.cargo.ExportProductService;
import com.itheima.service.cargo.ExportService;
import com.itheima.web.controller.BaseController;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/cargo/export")
public class ExportController extends BaseController {

    @Reference(check = false,retries = 0)
    private ContractService contractService;

    @Reference(check = false,retries = 0)
    private ExportService exportService;

    @Reference(check = false,retries = 0)
    private ExportProductService exportProductService;

    @RequestMapping("/contractList")
    public String goContractlist(@RequestParam(defaultValue = "1")int pageNum,
                         @RequestParam(defaultValue = "5")int pageSize){
        ContractExample contractExample = new ContractExample();
        contractExample.createCriteria().andCompanyIdEqualTo(getLoginCompanyId()).andStateEqualTo(1);
        PageInfo<Contract> pageInfo = contractService.findByPage(contractExample, pageNum, pageSize);
        request.setAttribute("pageInfo",pageInfo);
        return "cargo/export/export-contractList";
    }

    @RequestMapping("/toExport")
    public String toExport(String id){
        request.setAttribute("id",id);

        return "cargo/export/export-toExport";
    }

    @RequestMapping("list")
    public String golist(@RequestParam(defaultValue = "1")int pageNum,
                         @RequestParam(defaultValue = "5")int pageSize){
        ExportExample exportExample = new ExportExample();
        exportExample.createCriteria().andCompanyIdEqualTo(getLoginCompanyId());
        request.setAttribute("pageInfo",exportService.findByPage(exportExample,pageNum,pageSize));

        return "cargo/export/export-list";

    }

    @RequestMapping("/edit")
    public String edit(Export export){
        export.setCompanyId(getLoginCompanyId());
        export.setCompanyName(getLoginCompanyName());
        if (StringUtil.isEmpty(export.getId())){

            exportService.save(export);
        }else{
            exportService.update(export);
        }
        return "redirect:/cargo/export/list.do";
    }

    @RequestMapping("/toView")
    public String goView(String id){
        request.setAttribute("export",exportService.findById(id));
        return "cargo/export/export-view";
    }

    @RequestMapping("/toUpdate")
    public String goUpdate(String id){
        Export export = exportService.findById(id);
        request.setAttribute("export",export);

        ExportProductExample exportProductExample = new ExportProductExample();
        exportProductExample.createCriteria().andExportIdEqualTo(id);
        List<ExportProduct> eps = exportProductService.findAll(exportProductExample);
        request.setAttribute("eps",eps);
        return "cargo/export/export-update";
    }

    @RequestMapping("/submit")
    public String submit(String id){
        Export export = exportService.findById(id);
        if (export.getState()==0){
            export.setState(1);
        }
        exportService.update(export);
        return "redirect:/cargo/export/list.do";
    }

    @RequestMapping("/cancel")
    public String cancel(String id){
        Export export = exportService.findById(id);
        if (export.getState()==1) {
            export.setState(0);
        }
        exportService.update(export);
        return "redirect:/cargo/export/list.do";
    }

    @RequestMapping("/exportE")
    public String exportE(String id){

        Export export = exportService.findById(id);
        ExportVo exportVo = new ExportVo();
        BeanUtils.copyProperties(export,exportVo);
        exportVo.setExportId(export.getId());

        ExportProductExample exportProductExample = new ExportProductExample();
        exportProductExample.createCriteria().andExportIdEqualTo(id);
        List<ExportProduct> exportProductList = exportProductService.findAll(exportProductExample);
        ArrayList<ExportProductVo> exportProductVos = new ArrayList<>();
        for (ExportProduct exportProduct : exportProductList) {
            ExportProductVo exportProductVo = new ExportProductVo();
            BeanUtils.copyProperties(exportProduct,exportProductVo);
            exportProductVo.setExportProductId(exportProduct.getId());
            exportProductVos.add(exportProductVo);
        }
        exportVo.setProducts(exportProductVos);

        WebClient.create("http://localhost:9001/ws/export/user").post(exportVo);
        ExportResult exportResult = WebClient.create("http://localhost:9001/ws/export/user/" + id).get(ExportResult.class);
        exportService.updateExport(exportResult);
        return "redirect:/cargo/export/list.do";
    }



}
