package com.itheima.web.controller.system;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import com.itheima.service.system.DeptService;
import com.itheima.service.system.RoleService;
import com.itheima.service.system.UserService;
import com.itheima.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */

@Controller
@RequestMapping("/system/user")
public class UserController extends BaseController {

    @Autowired
    private DeptService deptService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @RequestMapping("/list")
    @RequiresPermissions("用户管理")
    public  String goList(@RequestParam(defaultValue = "1") int pageNum,
                          @RequestParam(defaultValue = "5") int pageSize){
        /*SecurityUtils.getSubject().checkPermission("用户管理");*/
        PageInfo<User> pageInfo = userService.findByPage(getLoginCompanyId(), pageNum, pageSize);
        request.setAttribute("pageInfo",pageInfo);
        return "system/user/user-list";
    }

    @RequestMapping("/delete.do")
    @ResponseBody
    public HashMap<String, String> delete(String id){
        HashMap<String, String> map = new HashMap<>();
        if (userService.delete(id)){
            map.put("message","删除成功！");
        }else {
            map.put("message","删除失败，当前删除用户被外键引用");
        }
        return map;
    }

    @RequestMapping("/toAdd.do")
    public String gotoAdd(){
        request.setAttribute("deptList",deptService.findAll(getLoginCompanyId()));

        return "system/user/user-add";
    }

    @RequestMapping("/toUpdate.do")
    public String goUpdate(String id){
        request.setAttribute("user",userService.findById(id));
        request.setAttribute("deptList",deptService.findAll(getLoginCompanyId()));
        return "system/user/user-update";
    }

    @RequestMapping("/edit.do")
    public String edit(User user){
        user.setCompanyId(getLoginCompanyId());
        user.setCompanyName(getLoginCompanyName());
        if (StringUtil.isEmpty(user.getId())){
            user.setId(UUID.randomUUID().toString());
            System.out.println(user);
            userService.save(user);
            String email = user.getEmail();
            String subject = "新员工入职通知";
            String content = "欢迎你来到SaasExport大家庭，我们是一个充满激情的团队，不是996哦!";
            HashMap<String, String> map = new HashMap<>();
            map.put("to","1021147610@qq.com");
            map.put("subject",subject);
            map.put("content",content);
            rabbitTemplate.convertAndSend("msg.email",map);
        }else {
            userService.update(user);
        }
        return "redirect:/system/user/list.do";
    }

    @RequestMapping("/roleList.do")
    public String goRoleList(String id){
        User user = userService.findById(id);

        List<Role> roleList = roleService.findAll(getLoginCompanyId());

        List<Role> userRoles =roleService.findRoleByUserId(id);
        StringBuilder userRoleStr = new StringBuilder();
        for (Role userRole : userRoles) {
            userRoleStr.append(userRole.getId()).append(",");
        }
        request.setAttribute("user",user);
        request.setAttribute("roleList",roleList);
        request.setAttribute("userRoleStr",userRoleStr.toString());
        return "system/user/user-role";
    }

    @RequestMapping("/changeRole.do")
    public String changeRole(String userid,String roleIds){

        userService.updateUserRole(userid,roleIds);

        return "redirect:/system/user/list.do";

    }


}
