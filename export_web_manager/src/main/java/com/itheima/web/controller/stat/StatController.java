package com.itheima.web.controller.stat;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.stat.StatService;
import com.itheima.web.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/stat")
public class StatController extends BaseController {
    @Reference
    private StatService statService;

    @RequestMapping("/toCharts")
    public String toCharts(String chartsType){
        return "stat/stat-"+chartsType;
    }

    @RequestMapping("/factoryCharts")
    @ResponseBody
    public List<Map<String,Object>> getFactoryData(){
        List<Map<String, Object>> factoryData = statService.getFactoryData(getLoginCompanyId());
        return factoryData;
    }

    @RequestMapping("/sellCharts")
    @ResponseBody
    public List<Map<String,Object>> getSellData(){
        return statService.getSellData(getLoginCompanyId());
    }

    @RequestMapping("/onlineCharts")
    @ResponseBody
    public List<Map<String,Object>> getOnlineData(){
        return statService.getOnlineData(getLoginCompanyId());
    }
    /*
    * 返回的json
    * [{"VALUE":1,"NAME":"00"},{"VALUE":0,"NAME":"01"},{"VALUE":0,"NAME":"02"},
    * {"VALUE":0,"NAME":"03"},{"VALUE":0,"NAME":"04"},{"VALUE":0,"NAME":"05"},
    * {"VALUE":0,"NAME":"06"},{"VALUE":0,"NAME":"07"},{"VALUE":25,"NAME":"08"},
    * {"VALUE":277,"NAME":"09"},{"VALUE":344,"NAME":"10"},{"VALUE":151,"NAME":"11"},
    * {"VALUE":21,"NAME":"12"},{"VALUE":152,"NAME":"13"},{"VALUE":210,"NAME":"14"},
    * {"VALUE":235,"NAME":"15"},{"VALUE":182,"NAME":"16"},{"VALUE":177,"NAME":"17"},
    * {"VALUE":108,"NAME":"18"},{"VALUE":2,"NAME":"19"},{"VALUE":28,"NAME":"20"},
    * {"VALUE":12,"NAME":"21"},{"VALUE":25,"NAME":"22"},{"VALUE":135,"NAME":"23"}]
    * */

}
