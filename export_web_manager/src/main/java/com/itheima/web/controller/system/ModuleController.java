package com.itheima.web.controller.system;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Module;
import com.itheima.service.system.DeptService;
import com.itheima.service.system.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/system/module")
public class ModuleController {

    @Autowired
    private ModuleService moduleService;
    @Autowired
    private DeptService deptService;

    /**
     * 模块列表分页
     */
    @RequestMapping("/list")
    public ModelAndView list(
            @RequestParam(defaultValue = "1") int pageNum,
            @RequestParam(defaultValue = "5") int pageSize) {
        //1.调用service查询部门列表
        PageInfo<Module> pageInfo = moduleService.findByPage(pageNum,
                pageSize);
        //2.返回
        ModelAndView mv = new ModelAndView();
        mv.addObject("pageInfo", pageInfo);
        mv.setViewName("system/module/module-list");
        return mv;
    }

    /**
     * 进入新增模块页面
     */
    @RequestMapping("/toAdd")
    public ModelAndView toAdd() {
        //1.查询所用模块数据，为了构造下拉框数据
        List<Module> list = moduleService.findAll();
        //2.返回
        ModelAndView mv = new ModelAndView();
        mv.addObject("menus", list);
        mv.setViewName("system/module/module-add");
        return mv;
    }

    /**
     * 新增或修改
     */
    @RequestMapping("/edit")
    public String edit(Module module) {
        //1.判断是否具有id属性
        if (StringUtils.isEmpty(module.getId())) {
            //2.没有id，保存
            moduleService.save(module);
        } else {
            //3.有id，更新
            moduleService.update(module);
        }
        return "redirect:/system/module/list.do";
    }

    /**
     * 进入到修改界面
     * 1.获取到id
     * 2.根据id进行查询
     * 3.查询所有的模块
     * 4.保存到request域中
     * 5.跳转到修改界面
     */
    @RequestMapping("/toUpdate")
    public ModelAndView toUpdate(String id) {
        //查询所用模块数据，为了构造下拉框数据
        List<Module> list = moduleService.findAll();
        //根据id查询
        Module module = moduleService.findById(id);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("system/module/module-update");
        mv.addObject("menus", list);
        mv.addObject("module", module);
        return mv;
    }

    /**
     * 删除模块
     */
    @RequestMapping("/delete")
    @ResponseBody
    public HashMap delete(String id) {
        HashMap<String, String> map = new HashMap<>();
        if (moduleService.delete(id)){
            map.put("message","删除成功");
        }else {
            map.put("message","删除失败，该数据使用中");
        }
        //跳转到修改界面
        return map;
    }


}
