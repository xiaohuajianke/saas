package com.itheima.web.controller.cargo;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.cargo.ContractProduct;
import com.itheima.domain.cargo.ContractProductExample;
import com.itheima.domain.cargo.FactoryExample;
import com.itheima.service.cargo.ContractProductService;
import com.itheima.service.cargo.FactoryService;
import com.itheima.web.Utils.FileUploadUtil;
import com.itheima.web.controller.BaseController;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/cargo/contractProduct")
public class ContractProductController extends BaseController {
    @Reference(check = false, retries = 0)
    private FactoryService factoryService;
    @Reference(check = false, retries = 0)
    private ContractProductService contractProductService;


    @RequestMapping("/list")
    public String goList(String contractId,
                         @RequestParam(defaultValue = "1") int pageNum,
                         @RequestParam(defaultValue = "5") int pageSize) {
        FactoryExample factoryExample = new FactoryExample();
        FactoryExample.Criteria criteria = factoryExample.createCriteria();
        criteria.andCtypeEqualTo("货物");
        request.setAttribute("factoryList", factoryService.findAll(factoryExample));

        ContractProductExample contractProductExample = new ContractProductExample();
        ContractProductExample.Criteria contractProductExampleCriteria = contractProductExample.createCriteria();
        contractProductExampleCriteria.andContractIdEqualTo(contractId);
        PageInfo<ContractProduct> pageInfo = contractProductService.findByPage(contractProductExample, pageNum, pageSize);
        request.setAttribute("pageInfo", pageInfo);
        request.setAttribute("contractId", contractId);

        return "cargo/product/product-list";
    }

    @RequestMapping("/edit")
    public String edit(ContractProduct contractProduct, MultipartFile productPhoto) {

        System.out.println(request.getAttribute("productPhoto"));
        contractProduct.setCompanyId(getLoginCompanyId());
        contractProduct.setCompanyName(getLoginCompanyName());
        if (StringUtils.isEmpty(contractProduct.getId())) {
            System.out.println(productPhoto);
            if (productPhoto!=null){
                String img = FileUploadUtil.upload(productPhoto);
                System.out.println(img);
                contractProduct.setProductImage(img);
            }

            contractProductService.save(contractProduct);
        } else {
            contractProductService.update(contractProduct);
        }

        return "redirect:/cargo/contractProduct/list.do?contractId=" + contractProduct.getContractId();

    }

    @RequestMapping("/toUpdate")
    public String update(String id) {
        ContractProduct contractProduct = contractProductService.findById(id);
        request.setAttribute("contractProduct", contractProduct);
        FactoryExample factoryExample = new FactoryExample();
        factoryExample.createCriteria().andCtypeEqualTo("货物");
        request.setAttribute("factoryList", factoryService.findAll(factoryExample));
        return "cargo/product/product-update";
    }

    @RequestMapping("/delete")
    public String delete(String id, String contractId) {

        contractProductService.delete(id);

        return "redirect:/cargo/contractProduct/list.do?contractId=" + contractId;
    }

    @RequestMapping("/toImport")
    public String goImport(String contractId) {
        request.setAttribute("contractId", contractId);
        return "cargo/product/product-import";
    }

    @RequestMapping("/import")
    public String importPro(MultipartFile file, String contractId) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = sheet.getRow(i);
            ContractProduct contractProduct = new ContractProduct();

            contractProduct.setFactoryName(row.getCell(1).getStringCellValue());
            contractProduct.setProductNo(row.getCell(2).getStringCellValue());
            contractProduct.setCnumber(((Double) (row.getCell(3).getNumericCellValue())).intValue());
            contractProduct.setPackingUnit(row.getCell(4).getStringCellValue());
            contractProduct.setLoadingRate(row.getCell(5).getStringCellValue());
            contractProduct.setBoxNum(((Double) row.getCell(6).getNumericCellValue()).intValue());
            contractProduct.setPrice((Double) row.getCell(7).getNumericCellValue());
            contractProduct.setProductRequest(row.getCell(8).getStringCellValue());
            contractProduct.setProductDesc(row.getCell(9).getStringCellValue());
            contractProduct.setCompanyId(getLoginCompanyId());
            contractProduct.setCompanyName(getLoginCompanyName());
            contractProduct.setContractId(contractId);
            contractProduct.setFactoryId(factoryService.findFactoryIdByName(contractProduct.getFactoryName()));
            contractProductService.save(contractProduct);

        }
        return "cargo/product/product-import";
    }

    @RequestMapping("/importJJ")
    public String importProduct(MultipartFile file, String contractId) {
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
            XSSFSheet sheet = workbook.getSheetAt(0);
            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                XSSFRow row = sheet.getRow(i);
                Object[] objs = new Object[10];
                for (int j = 1; j < row.getLastCellNum() + 1; j++) {
                    XSSFCell cell = row.getCell(j);
                    if (cell != null) {
                        objs[j] = getCellValue(cell);
                    }
                }
                ContractProduct contractProduct = new ContractProduct(objs, getLoginCompanyId(), getLoginCompanyName());
                contractProduct.setContractId(contractId);
                contractProductService.save(contractProduct);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "cargo/product/product-import";
    }

    private Object getCellValue(XSSFCell cell) {

        CellType cellType = cell.getCellType();
        Object result = null;
        switch (cellType) {
            case STRING: {
                result = cell.getStringCellValue();
                break;
            }
            case NUMERIC: {
                if (DateUtil.isCellDateFormatted(cell)) {
                    result = cell.getDateCellValue();
                } else {
                    result = cell.getNumericCellValue();
                }
                break;
            }
            case BOOLEAN: {
                result = cell.getBooleanCellValue();
                break;
            }
            default:
                break;
        }
        return result;
    }

}
