package com.itheima.web.controller.system;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.itheima.domain.system.Dept;
import com.itheima.service.system.DeptService;
import com.itheima.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("/system/dept")
public class DeptController extends BaseController {

    @Autowired
    private DeptService deptService;

    @RequestMapping("/list")
    public ModelAndView list(@RequestParam(defaultValue = "5") int pageSize,
                             @RequestParam(defaultValue = "1") int pageNum){
        String companyId=getLoginCompanyId();
        PageInfo<Dept> pageInfo = deptService.findByPage(companyId, pageSize, pageNum);
        ModelAndView mav = new ModelAndView();
        mav.addObject("pageInfo",pageInfo);
        mav.setViewName("system/dept/dept-list");
        return mav;
    }
    @RequestMapping("/toUpdate.do")
    public ModelAndView goUpdate(String id){
        String companyId=getLoginCompanyId();
        List<Dept> deptList = deptService.findAll(companyId);
        Dept dept = deptService.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("deptList",deptList);
        modelAndView.addObject("dept",dept);
        modelAndView.setViewName("system/dept/dept-update");
        return modelAndView;
    }

    @RequestMapping("/toAdd.do")
    public ModelAndView goAdd(){
        String companyId=getLoginCompanyId();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("deptList",deptService.findAll(companyId));
        modelAndView.setViewName("/system/dept/dept-add");
        return modelAndView;
    }

    @RequestMapping("/edit.do")
    public String edit(Dept dept){
        dept.setCompanyId(getLoginCompanyId());
        dept.setCompanyName(getLoginCompanyName());
        if (StringUtil.isEmpty(dept.getId())){
            dept.setId(UUID.randomUUID().toString());
            deptService.add(dept);
        }else {
            deptService.save(dept);
        }

        return "redirect:/system/dept/list.do";
    }

    @RequestMapping("/delete.do")
    @ResponseBody
    public HashMap<String, String> delete(String id){
        HashMap<String, String> result = new HashMap<>();
        if (deptService.removeById(id)){
            result.put("message","删除成功！");
        }else {
            result.put("message","删除失败！");
        }
        return result;
    }


}
