package com.itheima.web.controller;

import com.itheima.domain.system.Module;
import com.itheima.domain.system.User;
import com.itheima.service.system.ModuleService;
import com.itheima.service.system.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
public class LoginController extends BaseController{

    @Autowired
    private UserService userService;

    @Autowired
    private ModuleService moduleService;

    /*@RequestMapping("/login.do")
    public String login(String email,String password){
        if (StringUtil.isEmpty(email)||StringUtil.isEmpty(password)){
            return "redirect:/login.jsp";
        }
        User user=userService.findByEmail(email);
        System.out.println(user);
        if (user!=null&&user.getPassword().equals(password)){
            session.setAttribute("loginUser",user);
            List<Module> modules= moduleService.findModuleByUserId(user.getId());
            session.setAttribute("modules",modules);
            return "home/main";
        }
        session.setAttribute("error","用户名或密码错误，请重新登录！");
        return "redirect:/login.jsp";
    }*/

    @RequestMapping("/login.do")
    public String login(String email,String password){

        try {
            Subject subject = SecurityUtils.getSubject();

            UsernamePasswordToken upToken = new UsernamePasswordToken(email, password);

            subject.login(upToken);

            User user = (User)subject.getPrincipal();

            session.setAttribute("loginUser",user);
            List<Module> modules= moduleService.findModuleByUserId(user.getId());
            session.setAttribute("modules",modules);
            return "home/main";
        }catch (UnknownAccountException ue){
            ue.printStackTrace();
            session.setAttribute("error","用户名不存在");
            return "forward:login.jsp";

        }catch (IncorrectCredentialsException ice){
            ice.printStackTrace();
            session.setAttribute("error","密码错误");
            return "forward:login.jsp";
        }catch (Exception e){
            e.printStackTrace();
            session.setAttribute("error","用户名或密码错误，请重新登录！");
            return "forward:login.jsp";
        }
    }

    @RequestMapping("/home.do")
    public String goHome(){
        return "home/home";
    }


    @RequestMapping("/logout.do")
    public String loginout(){
        SecurityUtils.getSubject().logout();
        /*session.removeAttribute("loginUser");
        session.invalidate();*/
        session.removeAttribute("loginUser");
        return "forward:login.jsp";
    }

}
