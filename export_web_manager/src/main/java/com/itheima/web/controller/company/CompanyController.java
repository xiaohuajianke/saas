package com.itheima.web.controller.company;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.company.Company;
import com.itheima.service.company.CompanyService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Controller
@RequestMapping("company")
public class CompanyController {

    @Reference(check = false,retries = 0)
    private CompanyService companyService;

    @RequestMapping("/list")
    public String list(@RequestParam(defaultValue = "1") int pageNum,
                       @RequestParam(defaultValue = "5")int pageSize,HttpServletRequest request){
        PageInfo<Company> pageInfo = companyService.findByPage(pageNum, pageSize);
        request.setAttribute("pageInfo",pageInfo);

        return "company/company-list";
    }

    @RequestMapping("/toAdd.do")
    public String goAdd(){
        return "company/company-add";
    }

    @RequestMapping("/edit.do")
    public String edit(Company company){
        if (company.getId()==null||company.getId().equals("")){
            company.setId(UUID.randomUUID().toString());
            System.out.println(company);
            companyService.add(company);

        }else {
            System.out.println(company);
            companyService.update(company);
        }
        return "redirect:/company/list.do";
    }

    @RequestMapping("/toUpdate.do")
    public String goUpdate(HttpServletRequest request,String id){
        request.setAttribute("company",companyService.findCompanyById(id));
        return "company/company-update";
    }

    @RequestMapping("/delete.do")
    public String deleteById(String id){
        companyService.deleteById(id);
        return "redirect:/company/list.do";
    }



}
