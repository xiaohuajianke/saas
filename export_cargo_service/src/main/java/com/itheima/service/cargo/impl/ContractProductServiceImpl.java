package com.itheima.service.cargo.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.cargo.ContractDao;
import com.itheima.dao.cargo.ContractProductDao;
import com.itheima.dao.cargo.ExtCproductDao;
import com.itheima.domain.cargo.*;
import com.itheima.domain.vo.ContractProductVo;
import com.itheima.service.cargo.ContractProductService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Service(timeout = 100000)
public class ContractProductServiceImpl implements ContractProductService {

    @Autowired
    private ContractProductDao contractProductDao;

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private ExtCproductDao extCproductDao;


    @Override
    public PageInfo<ContractProduct> findByPage(ContractProductExample example, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<ContractProduct> list = contractProductDao.selectByExample(example);

        return new PageInfo<>(list);
    }

    @Override
    public List<ContractProduct> findAll(ContractProductExample example) {
        return contractProductDao.selectByExample(example);
    }

    @Override
    public ContractProduct findById(String id) {
        return contractProductDao.selectByPrimaryKey(id);
    }

    @Override
    public void save(ContractProduct contractProduct) {
        contractProduct.setId(UUID.randomUUID().toString());
        double amount=0d;
        if (contractProduct.getCnumber()!=null&&contractProduct.getPrice()!=null)
            amount=contractProduct.getCnumber()*contractProduct.getPrice();
            contractProduct.setAmount(amount);
        contractProductDao.insertSelective(contractProduct);
        Contract contract = contractDao.selectByPrimaryKey(contractProduct.getContractId());
        contract.setProNum(contract.getProNum()+1);
        contract.setTotalAmount(contract.getTotalAmount()+amount);
        contractDao.updateByPrimaryKeySelective(contract);

    }

    @Override
    public void update(ContractProduct contractProduct) {

        ContractProduct oldCP = contractProductDao.selectByPrimaryKey(contractProduct.getId());
        double amount=0d;
        if (contractProduct.getCnumber()!=null&&contractProduct.getPrice()!=null){
            amount=contractProduct.getCnumber()*contractProduct.getPrice();
        }
        contractProduct.setAmount(amount);
        contractProductDao.updateByPrimaryKeySelective(contractProduct);

        Contract contract = contractDao.selectByPrimaryKey(contractProduct.getContractId());
        contract.setTotalAmount(contract.getTotalAmount()-oldCP.getAmount()+amount);
        contractDao.updateByPrimaryKeySelective(contract);

        contractProductDao.updateByPrimaryKeySelective(contractProduct);
    }

    @Override
    public void delete(String id) {

        ContractProduct contractProduct = contractProductDao.selectByPrimaryKey(id);
        contractProductDao.deleteByPrimaryKey(id);

        double amount=contractProduct.getAmount();

        ExtCproductExample extCproductExample = new ExtCproductExample();
        extCproductExample.createCriteria().andContractProductIdEqualTo(id);
        List<ExtCproduct> extCproducts = extCproductDao.selectByExample(extCproductExample);
        for (ExtCproduct extCproduct : extCproducts) {
            amount+=extCproduct.getAmount();
            extCproductDao.deleteByPrimaryKey(extCproduct.getId());
        }


        Contract contract = contractDao.selectByPrimaryKey(contractProduct.getContractId());
        contract.setTotalAmount(contract.getTotalAmount()-amount);
        contract.setProNum(contract.getProNum()-1);
        contract.setExtNum(contract.getExtNum()-extCproducts.size());
        contractDao.updateByPrimaryKeySelective(contract);

    }

    @Override
    public List<ContractProductVo> findByShipTime(String companyId, String inputDate) {
        return contractProductDao.findByShipTime(companyId,inputDate);
    }
}
