package com.itheima.service.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.itheima.dao.system.RoleDao;
import com.itheima.domain.system.Role;
import com.itheima.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * 观自在菩萨，行深般若波罗蜜多时，照见五蕴皆空，度一切苦厄。
 * 舍利子，色不异空，空不异色，色即是空，空即是色，受想行识亦复如是。
 * 舍利子，是诸法空相，不生不灭，不垢不净，不增不减。
 * 是故空中无色，无受想行识，无眼耳鼻舌身意，无色声香味触法，无眼界乃至无意识界，无无明亦无无明尽，乃至无老死，亦无老死尽，无苦集灭道，无智亦无得。
 * 以无所得故，菩提萨埵，依般若波罗蜜多故，心无挂碍；无挂碍故，无有恐怖，远离颠倒梦想，究竟涅槃。
 * 三世诸佛，依般若波罗蜜多故，得阿耨多罗三藐三菩提。
 * 故知般若波罗蜜多，是大神咒，是大明咒，是无上咒，是无等等咒，能除一切苦，真实不虚。
 * 故说般若波罗蜜多咒，即说咒曰：揭谛揭谛，波罗揭谛，波罗僧揭谛，菩提萨婆诃。
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    public Role findById(String id) {
        return roleDao.findById(id);
    }
    //分页
    public PageInfo<Role> findByPage(String companyId, int pageNum, int
            pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Role> list = roleDao.findAll(companyId);
        return new PageInfo<>(list);
    }
    public boolean delete(String id) {
        Long count = roleDao.findUserRoleByRoleId(id);
        if (count>0){
           return false;
        }else {
            roleDao.delete(id);
            return true;
        }

    }

    public void save(Role role) {
        //指定id属性
        role.setId(UUID.randomUUID().toString());
        roleDao.save(role);
    }
    public void update(Role role) {
        roleDao.update(role);
    }

    @Override
    public void updateRoleModule(String roleid, String moduleIds) {
        roleDao.deleteRoleModuleByRoleId(roleid);
        System.out.println("moduleIds"+moduleIds);
        if (!StringUtil.isEmpty(moduleIds)){
            String[] mids = moduleIds.split(",");
            if (mids!=null){
                for (String moduleId : mids) {
                    roleDao.addRoleModule(roleid,moduleId);
                }
            }
        }
    }

    @Override
    public List<Role> findAll(String id) {
        return roleDao.findAll(id);
    }

    @Override
    public List<Role> findRoleByUserId(String id) {
        return roleDao.findRoleByUserId(id);
    }
}
