package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Module;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * █████   ▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██    ▒██   ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 * ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 * ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 * ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 * ░     ░ ░      ░  ░
 */
@Service
public interface UserService {

    // 分页查询
    PageInfo<User> findByPage(String companyId, int pageNum, int PageSize);
    //查询所有部门
    List<User> findAll(String companyId);
    //保存
    void save(User user);
    //更新
    void update(User user);
    //删除
    boolean delete(String id);
    //根据id查询
    User findById(String id);

    void deleteUserRole(String userId);

    void updateUserRole(String userId,String roleIds);

    User findByEmail(String email);

    List<Module> findModulesByUserId(String id);
}
